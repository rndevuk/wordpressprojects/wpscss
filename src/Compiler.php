<?php
namespace Rndevuk\Wpscss;

use ScssPhp\ScssPhp\Compiler as ScssPhpCompiler;

class Compiler {

    public $scss_dir;
    public $css_dir;
    public $cache_dir;
    public $scssphp;

    public function __construct (string $theme_path = '', array $variables = [])
    {
        $this->scss_dir     = $theme_path . '/assets/scss/';
        $this->css_dir      = $theme_path . '/assets/css/';
        $this->cache_dir    = $theme_path . '/assets/cache/';
        $this->scssphp      = new ScssPhpCompiler();

        if (defined('WP_DEBUG') && WP_DEBUG == true)
        {
            $this->scssphp->setFormatter('ScssPhp\ScssPhp\Formatter\Expanded');
        }else{
            $this->scssphp->setFormatter('ScssPhp\ScssPhp\Formatter\Crunched');
        }

        $this->scssphp->setImportPaths($this->scss_dir);

        $this->scssphp->addImportPath(function($path) use ($theme_path) {
            if (!file_exists($theme_path . '/assets/vendor/')) return;
            return $theme_path . '/assets/vendor/';
        });

        $this->set_variables($variables);

        if(isset($_GET['SASS']) || (defined('WP_DEBUG') && WP_DEBUG == true))
        {
            $this->compile();
        }
    }

    public function compile()
    {
        $in     = $this->scss_dir.'/style.scss';
        $out    = $this->css_dir.'/style.css';

        if (is_writable($this->css_dir))
        {
            try {
                $css = $this->scssphp->compile(file_get_contents($in));
                file_put_contents($out, $css);
            } catch (\Exception $e) {
                return new \WP_Error('broke', $in .' - '. $e->getMessage() . PHP_EOL);
            }
        } else {
            return new \WP_Error('broke', $out .' - File Permission Error, permission denied. Please make the css directory writable.' . PHP_EOL);
        }
    }

    public function set_variables(array $variables)
    {
        $this->scssphp->setVariables($variables);
    }

}
